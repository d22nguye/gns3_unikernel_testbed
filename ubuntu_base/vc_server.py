import socket, struct
import time
import threading, sys

# Configuration
LISTEN_PORT = 12346  # Specify the port number to listen on
num_security_node = 0
MAX_SECURITY_NODES = 0
node_index_id = 0
server_round = 0
map_security_node = {}


class State:
    INIT = 0
    COMPUTING = 1
    DECIDED_IN_VC = 2
    DECIDED_NOT_VC = 3


def listen_for_node(id_node, client_socket):
    global server_round

    while True:
        data = client_socket.recv(8)  # Expecting 1 byte
        if data:
            state, round_val = struct.unpack('!II', data)
            print(f"Received in id: {id_node} - state: {state} - round: {round_val}")

            if map_security_node[id_node]["state"] == state and map_security_node[id_node]["round"] == round_val:
                print("Error!! Receive the same message")
            else:
                map_security_node[id_node]["state"] = state
                map_security_node[id_node]["round"] = round_val

                if state == State.DECIDED_IN_VC or state == State.DECIDED_NOT_VC:
                    client_socket.close()
                    break


def print_mapping_state():
    global map_security_node

    for id_node in map_security_node.keys():
        node = map_security_node[id_node]
        if node["state"] == State.DECIDED_NOT_VC:
            print("Node", id_node, "is NOT in VC")
        elif node["state"] == State.DECIDED_IN_VC:
            print("Node", id_node, "is IN VC")
        else:
            print("Node", id_node, "is", node["state"])


def check_start_new_round():
    global server_round, map_security_node

    is_no_compute = True
    for node in map_security_node.values():
        if node["state"] == State.COMPUTING:
            is_no_compute = False
            if node["round"] != server_round:
                return 0
        elif node["state"] == State.INIT:
            return 0

    if is_no_compute:
        return 2

    return 1


def start_new_round():
    global server_round
    while True:
        start = check_start_new_round()
        if start == 1:
            server_round += 1
            for node in map_security_node.values():
                if node["state"] == State.COMPUTING:
                    client_socket = node["socket"]
                    message = struct.pack('!I', server_round)
                    client_socket.sendall(message)
            print("Start new round:", server_round)
        elif start == 2:
            print_mapping_state()
            break
        # print("Error!! Cannot start new round!")
        time.sleep(1)


def main():
    global num_security_node, node_index_id, MAX_SECURITY_NODES, map_security_node

    if len(sys.argv) > 2:
        MAX_SECURITY_NODES = int(sys.argv[1])
        delta = int(sys.argv[2])
    else:
        return

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(('0.0.0.0', LISTEN_PORT))
        server_socket.listen()

        print(f"Listening for incoming connections on port {LISTEN_PORT}...")

        while num_security_node < MAX_SECURITY_NODES:
            client_socket, client_address = server_socket.accept()
            data = client_socket.recv(8)  # Expecting 1 byte
            if data:
                # Unpack the boolean and integer
                state, round_val = struct.unpack('!II', data)
                print(f"Received state: {state}, round: {round_val}")

                num_security_node += 1
                node_index_id += 1
                id_to_send = node_index_id

                map_security_node[node_index_id] = {"state": state, "round": round_val, "socket": client_socket}

                thread = threading.Thread(target=listen_for_node, args=(id_to_send, client_socket))
                thread.daemon = True
                thread.start()

                response = struct.pack('!II', id_to_send, delta)
                client_socket.sendall(response)

        start_new_round()


if __name__ == "__main__":
    main()
