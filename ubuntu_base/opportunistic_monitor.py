import subprocess
from netfilterqueue import NetfilterQueue
from opportunistic_utils import *

map_IP_with_source_switch = {}


def process_packet(new_packet):
    data = new_packet.get_payload()

    # Parse the raw data into a Scapy IP packet
    pkt = IP(data)

    if pkt.haslayer(IP):
        # print(f"IP Packet: {pkt.summary()}")
        # pkt.show()

        index_custom_option = -1
        if pkt.options:
            for i in range(len(pkt.options)):
                option = pkt.options[i]

                if isinstance(option, CustomIPOption) and option.option == MY_OPTION_TYPE:
                    index_custom_option = i
                    for entry in option.read_all_entry():
                        # save ip address for later attacker, node_id and interface_num

                        if entry["cmd"] == Cmd.REGISTER:
                            print(f"Custom Option from REGISTER found: id = {entry['node_id']}, inter = ens{entry['inter_num']}")
                            map_IP_with_source_switch[pkt[IP].src] = {"node_id": entry['node_id'], "inter_num": entry['inter_num']}

        IP_attacker = "172.16.60.133"
        if pkt[IP].dst == IP_attacker and IP_attacker in map_IP_with_source_switch:
            info = map_IP_with_source_switch[IP_attacker]
            node_id = info["node_id"]
            inter_num = info["inter_num"]

            if index_custom_option == -1:
                option = CustomIPOption()
                option.add_entry(node_id, inter_num, Cmd.DECISION_DROP)
                if pkt.options:
                    pkt.options.append(option)  # Append to the existing options
                else:
                    pkt.options = [option]  # Add as the first option if none exist
            else:
                pkt.options[index_custom_option].add_entry(node_id, inter_num, Cmd.DECISION_DROP)

            del pkt.len
            del pkt.ihl
            del pkt[IP].chksum  # Invalidate the checksum
            new_packet.set_payload(bytes(pkt))

            # pkt.show()

    new_packet.accept()


def main():
    check_rule_command = "sudo iptables -C FORWARD -p tcp -j NFQUEUE --queue-num 1"

    try:
        # Run the check command
        subprocess.run(check_rule_command, shell=True, check=True)
        print("Rule already exists, no need to add it.")
    except subprocess.CalledProcessError:
        # If the rule doesn't exist, add it
        add_rule_command = "sudo iptables -I FORWARD -p tcp -j NFQUEUE --queue-num 1"
        try:
            subprocess.run(add_rule_command, shell=True, check=True)
            print("Rule added successfully.")
        except subprocess.CalledProcessError as e:
            print(f"Failed to add the rule: {e}")

    q = NetfilterQueue()
    q.bind(1, process_packet)  # Bind to queue number 1

    try:
        # Run the queue listener
        print("Listening for packets...")
        q.run()
    except KeyboardInterrupt:
        print("Stopping packet listener")


if __name__ == "__main__":
    main()
