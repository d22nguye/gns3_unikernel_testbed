from scapy.all import *
from scapy.layers.inet import IPOption, IP

MY_OPTION_TYPE = 31


class Cmd:
    DECISION_DROP = 0
    REGISTER = 1


class CustomIPOption(IPOption):
    name = "CustomIPOption"
    fields_desc = [
        ByteField("option", MY_OPTION_TYPE),  # Option type
        FieldLenField("length", 3, length_of="value", fmt="B"),  # Option length
        ByteField("count", 0),  # Number of entries in the array
        StrLenField("value", "", length_from=lambda pkt: pkt.length - 3),  # Array data (id, inter, cmd)
    ]

    # Custom methods to add entries (id, inter, cmd) to the value field
    def add_entry(self, id_val, inter_val, cmd_val):
        entry = struct.pack('!IBB', id_val, inter_val, cmd_val)
        self.value += entry
        self.count += 1  # Update the count field
        self.length += len(entry)

    # Custom method to read a specific entry by index
    def read_entry(self, index):
        if index < self.count:
            # Calculate the offset of the entry in the value field
            offset = index * 6  # Each entry is 6 bytes (1 integer, 2 bytes)
            entry_data = self.value[offset:offset + 6]
            id_val, inter_val, cmd_val = struct.unpack('!IBB', entry_data)
            return {"node_id": id_val, "inter_num": inter_val, "cmd": cmd_val}
        else:
            raise IndexError("Entry index out of range")

    # Custom method to read all entries at once
    def read_all_entry(self):
        entries = []
        for i in range(self.count):
            entry = self.read_entry(i)
            entries.append(entry)
        return entries
