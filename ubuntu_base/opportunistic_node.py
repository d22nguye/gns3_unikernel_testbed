from netfilterqueue import NetfilterQueue
from opportunistic_utils import *

debug = False
name_interface = "vmnet" if debug else "ens"
MIN_INTER_NUMBER = 1 if debug else 4
MAX_INTER_NUMBER = 2 if debug else 14
MARK_TO_INTERFACE = {}
for index in range(MIN_INTER_NUMBER, MAX_INTER_NUMBER):
    MARK_TO_INTERFACE[index] = name_interface + str(index)

my_id = int(sys.argv[1])
DIR = "/home/node/Desktop/gns3_unikernel_testbed"
map_firewall = {}


def setup_firewall(inter_num):
    try:
        subprocess.run(
            [f'{DIR}/ubuntu_base/configure_bridge_firewall', f"public{inter_num}", f"private{inter_num}", f"ens{inter_num}", f"br{inter_num}", f"{inter_num}", DIR],
            check=True)
    except subprocess.CalledProcessError as e:
        # This will run if the command returns a non-zero exit code (i.e., an error)
        print(f"Error: {e}")
        print(f"Return code: {e.returncode}")  # The exit code
        print(f"Command output: {e.output}")  # Any output (stdout/stderr)


def process_packet(new_packet):
    global my_id, name_interface

    inter_num = new_packet.get_mark()
    # Determine the incoming interface based on the mark
    # interface = MARK_TO_INTERFACE.get(inter_num, 'unknown')
    # print(interface)

    data = new_packet.get_payload()

    # Parse the raw data into a Scapy IP packet
    pkt = IP(data)

    # Check if it's a TCP packet
    if pkt.haslayer(IP):
        # print(f"IP Packet: {pkt.summary()}")
        # pkt.show()

        is_register_me = True
        index_custom_option = -1

        if pkt.options:
            for i in range(len(pkt.options)):
                option = pkt.options[i]
                if isinstance(option, CustomIPOption) and option.option == MY_OPTION_TYPE:
                    index_custom_option = i
                    for entry in option.read_all_entry():
                        if entry["cmd"] == Cmd.DECISION_DROP:

                            # Do something to deploy firewall
                            if entry["node_id"] == my_id and pkt[IP].dst not in map_firewall:
                                print(f"Custom Option from DECISION found: id = {entry['node_id']}, inter = {name_interface}{entry['inter_num']}, cmd = {entry['cmd']}")
                                new_firewall_thread = threading.Thread(target=setup_firewall, args=[entry['inter_num']])
                                new_firewall_thread.daemon = True
                                new_firewall_thread.start()

                                map_firewall[pkt[IP].dst] = entry['cmd']
                            else:
                                print("Firewall already deployed, no need to add")

                        elif entry["cmd"] == Cmd.REGISTER:
                            is_register_me = False

        if is_register_me:
            if index_custom_option == -1:
                option = CustomIPOption()
                option.add_entry(my_id, inter_num, Cmd.REGISTER)
                if pkt.options:
                    pkt.options.append(option)  # Append to the existing options
                else:
                    pkt.options = [option]  # Add as the first option if none exist
            else:
                pkt.options[index_custom_option].add_entry(my_id, inter_num, Cmd.REGISTER)

            del pkt.len
            del pkt.ihl
            del pkt[IP].chksum  # Invalidate the checksum
            new_packet.set_payload(bytes(pkt))
            # pkt.show()  # Now this will work correctly

    new_packet.accept()


def main():
    rule_check = "sudo iptables -C FORWARD -i br0 -p tcp -j NFQUEUE --queue-num 1"
    try:
        subprocess.run(rule_check, shell=True, check=True)
        print("Rule already exists, no need to add it.")
    except subprocess.CalledProcessError:
        try:
            rule_1 = "sudo iptables -I FORWARD -i br0 -p tcp -j NFQUEUE --queue-num 1"
            subprocess.run(rule_1, shell=True, check=True)

            for i in range(MIN_INTER_NUMBER, MAX_INTER_NUMBER):
                inter_name = MARK_TO_INTERFACE[i]
                rule_2 = f"sudo iptables -t mangle -A FORWARD -i br0 -m physdev --physdev-in {inter_name} -j MARK --set-mark {i}"
                subprocess.run(rule_2, shell=True, check=True)

            print("Rule added successfully.")
        except subprocess.CalledProcessError as e:
            print(f"Failed to add the rule: {e}")

    q = NetfilterQueue()
    q.bind(1, process_packet)  # Bind to queue number 1

    try:
        # Run the queue listener
        print("Listening for packets...")
        q.run()
    except KeyboardInterrupt:
        print("Stopping packet listener")


if __name__ == "__main__":
    main()
