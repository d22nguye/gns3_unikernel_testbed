import socket, struct, threading
import math, queue
import time
from scapy.all import sendp, sniff, Raw, get_if_hwaddr
from scapy.layers.l2 import Ether
import subprocess, netifaces, sys

# Configuration
LISTEN_PORT = 12346  # Specify the port number to listen on

delete_queue = queue.Queue()
debug = False
DIR = "/home/node/Desktop/gns3_unikernel_testbed"


class State:
    INIT = 0
    COMPUTING = 1
    DECIDED_IN_VC = 2
    DECIDED_NOT_VC = 3


class Cmd:
    PROP = 1
    REPLY_PROP = 2
    PROPOSE = 3
    ACCEPT = 4
    DROP_ME = 5


class VcNode:
    def __init__(self, state, degree, round_val, myid, interface):
        self.state = state
        self.degree = degree
        self.round_val = round_val
        self.myid = myid
        self.interface = interface


delta = 3
map_point = {}

node = VcNode(State.INIT, 0, 0, 0, [])


# Function to broadcast an Ethernet packet
def broadcast_packet(message):
    global node

    for iface in node.interface:
        src_mac = get_if_hwaddr(iface)
        dst_mac = "ff:ff:ff:ff:ff:ff"
        packet = Ether(src=src_mac, dst=dst_mac) / Raw(load=message)  # Broadcast MAC address
        sendp(packet, iface=iface)  # Specify the interface


# Function to send a packet to a specific MAC address
def send_packet(iface, dst_mac, message):
    src_mac = get_if_hwaddr(iface)
    packet = Ether(src=src_mac, dst=dst_mac) / Raw(load=message)
    sendp(packet, iface=iface)


def send_node_info(client_socket):
    global node
    message = struct.pack('!II', node.state, node.round_val)
    client_socket.sendall(message)


# Function to receive packets at Layer 2
def process_packet(iface, my_mac):
    global node, map_point

    def packet_callback(packet):
        if Ether in packet and Raw in packet and len(packet.layers()) == 2:
            # print(packet)
            if packet[Ether].dst == my_mac or (
                    packet[Ether].src != my_mac and packet[Ether].dst == "ff:ff:ff:ff:ff:ff"):
                raw_data = packet[Raw].load
                print("raw data from iface", iface, ":", raw_data)

                cmd = struct.unpack('!I', raw_data[:4])[0]

                if cmd == Cmd.PROP or cmd == Cmd.REPLY_PROP:
                    neighbor_id = struct.unpack('!I', raw_data[4:8])[0]
                    map_point[neighbor_id] = (packet[Ether].src, iface)
                    node.degree = len(map_point)
                    print(f"My degree now is: {node.degree}")

                    if cmd == Cmd.PROP:
                        message = struct.pack('!II', Cmd.REPLY_PROP, node.myid)
                        send_packet(iface, packet[Ether].src, message)
                elif cmd == Cmd.DROP_ME:
                    neighbor_id = struct.unpack('!I', raw_data[4:8])[0]
                    delete_queue.put(neighbor_id)

    return packet_callback
    # print(f"Received packet: {packet.summary()}")


def check_all_interfaces():
    try:
        # Run the 'ip addr' command and capture the output
        output = subprocess.check_output(['ip', 'addr'], text=True)

        # Split the output into lines
        lines = output.splitlines()
        interface_states = {}

        current_interface = None

        # Parse the output to find each interface and its state
        for line in lines:
            # Check for lines starting with a number (which indicate the interface index)
            if line and line[0].isdigit():
                # Extract the interface name (the first word in the line)
                current_interface = line.split(': ')[1].split()[0]
            # Check if the line indicates the state of the interface
            if current_interface and 'state' in line:
                state_iface = 'up' if 'state UP' in line else 'down' if 'state DOWN' in line else 'unknown'
                interface_states[current_interface] = state_iface
                current_interface = None  # Reset for the next interface

        return interface_states
    except subprocess.CalledProcessError as e:
        return f"Error executing command: {e}"


def drop_me():
    message = struct.pack('!II', Cmd.DROP_ME, node.myid)
    broadcast_packet(message)


def start_sniffing(iface):
    # my_mac = get_if_hwaddr(iface)
    sniff(iface=iface, prn=process_packet(iface, get_if_hwaddr(iface)), store=False)


def update_degree():
    global map_point, node

    while not delete_queue.empty():
        neighbor_id = delete_queue.get()
        del map_point[neighbor_id]
        node.degree = len(map_point)
        print(f"My degree after drop: {node.degree}")


def wait_for_ip(interface_name):
    while True:
        try:
            # Get the addresses for the specific interface
            addresses = netifaces.ifaddresses(interface_name)
            # Fetch the IPv4 address (AF_INET)
            ip_address = addresses[netifaces.AF_INET][0]['addr']
            if ip_address.startswith("192.168."):
                print(f"IP Address assigned: {ip_address}")
            else:
                raise Exception("Waiting for valid IP address")
            break
        except:
            print("No valid IP address assigned yet. Retrying...")
            time.sleep(5)  # Wait 5 seconds before checking again


def proceed_simple_VC():
    global node, delta, debug

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    if debug:
        client_socket.connect(('0.0.0.0', 12346))
    else:
        client_socket.connect(('192.168.1.10', 12346))

    # Start state INITIATE
    send_node_info(client_socket)

    response = client_socket.recv(8)
    received_id, delta = struct.unpack('!II', response)
    print(f"Received ID: {received_id}")
    print(f"delta: {delta}")
    node.myid = received_id

    # 1. Start get degree
    for iface, state_iface in check_all_interfaces().items():
        if iface != "lo" and iface != "ens3" and state_iface == "up":
            node.interface.append(iface)
            print("Listen to interface", iface)
            sniffer_thread = threading.Thread(target=start_sniffing, args=[iface])
            sniffer_thread.daemon = True
            sniffer_thread.start()

    message = struct.pack('!II', Cmd.PROP, node.myid)

    turn = 3
    while turn:
        broadcast_packet(message)
        turn -= 1
        time.sleep(2)

    # Start state COMPUTING
    node.state = State.COMPUTING

    send_node_info(client_socket)

    while True:
        data = client_socket.recv(4)
        server_round = struct.unpack('!I', data)[0]

        node.round_val += 1
        if node.round_val == server_round:
            if node.round_val > math.log2(delta):
                node.state = State.DECIDED_NOT_VC
                send_node_info(client_socket)
                break
            elif node.degree > (delta / (2.0 * node.round_val)):
                node.state = State.DECIDED_IN_VC
                drop_me()
                send_node_info(client_socket)
                break
            else:
                time.sleep(3)
                update_degree()
                send_node_info(client_socket)

        else:
            print("Error my round is", node.round_val, "but server round is", server_round)
    print("I am", "IN VC" if node.state == State.DECIDED_IN_VC else "NOT in VC")

    # Start a new Python process to run another script based on Node decision

    if node.state == State.DECIDED_IN_VC:
        subprocess.run(f"sudo {DIR}/ubuntu_base/./initiate_for_monitor", shell=True, check=True)
        print("./initiate_for_monitor is executed")
        subprocess.Popen([sys.executable, f'{DIR}/ubuntu_base/opportunistic_monitor.py'])
    elif node.state == State.DECIDED_NOT_VC:
        subprocess.run(f"sudo {DIR}/ubuntu_base/./initiate", shell=True, check=True)
        print("./initiate is executed")
        subprocess.Popen([sys.executable, f'{DIR}/ubuntu_base/opportunistic_node.py', str(node.myid)])
    else:
        print("Error I cannot decide!!")

    # Exit the current script
    sys.exit()


def main():
    global debug
    if not debug:
        wait_for_ip("ens3")
    proceed_simple_VC()


if __name__ == "__main__":
    main()
