import sys, time, os, json
import requests
from scapy.all import *
from scapy.layers.inet import IP
from scapy.layers.smb import SMB_Header

array_point = []

username = "admin"
password = "gjLdHjX0zOiN33qypgN5NDkCga5jeEmjZCWPqV8TtpRtJV4LuGz725nheLbEnyDv"

PROJECT_ID = ""
MAX_SIZE_OF_SWITCH_PORT = 53
MAX_SIZE_OF_UBUNTU_ADAPTER = 1
SWITCH_TYPE = 1
UBUNTU_TYPE = 2
WINDOW_TYPE = 3
ROUTER_TYPE = 4
HELLO_UNIKERNEL_TYPE = 5
UBUNTU_BASE_TYPE = 6

gnsmanager = None


class GNS3Manager:
    def get_projectId(self):
        global username, password
        url = "http://localhost:3080/v2/projects"
        response = requests.get(url, auth=(username, password))

        if response.status_code == 200:
            json_data = response.json()
            for project in json_data:
                if project["status"] == "opened":
                    print(project["filename"])
                    return project["project_id"], project["filename"]
        else:
            print(f"Error get_projectId: {response.status_code}")

    def link_host_to_host(self, host_id_1, adapter_number_1, port_number_1, host_id_2, adapter_number_2, port_number_2):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/links"

        data = {"nodes": [{"adapter_number": adapter_number_1, "node_id": host_id_1, "port_number": port_number_1},
                          {"adapter_number": adapter_number_2, "node_id": host_id_2, "port_number": port_number_2}]}
        response = requests.post(url, json=data, auth=(username, password))
        if response.status_code == 201:
            json_data = response.json()
        else:
            print(f"Error link_host_to_host: {response.status_code}")
            print("Error message:", response.text)

    def get_nodes(self):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/nodes"

        response = requests.get(url, auth=(username, password))

        if response.status_code == 200:
            json_data = response.json()

            array = []
            for i in json_data:
                array.append([i["node_id"], i["name"], i["x"], i["y"], i["z"]])
            return array
        else:
            print(f"Error get_nodes: {response.status_code}")

    def start_node(self, node_id):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/nodes/{node_id}/start"
        response = requests.post(url, auth=(username, password))
        if response.status_code == 200:
            print("start pc " + node_id)
        else:
            print(f"Error start_node: {response.status_code}")

    def create_switch(self, port_number, x, y, z):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/nodes"

        data = {'command_line': None, 'compute_id': 'local', 'console_auto_start': False, 'console_host': 'localhost',
                'custom_adapters': [], 'first_port_name': None, 'height': 32, 'label': {'rotation': 0,
                                                                                        'style': 'font-family: TypeWriter;font-size: 10.0;font-weight: bold;fill: #000000;fill-opacity: 1.0;',
                                                                                        'text': 'Switch', 'x': -30,
                                                                                        'y': -39}, 'locked': False,
                'name': 'Switch{0}', 'node_directory': None, 'node_type': 'ethernet_switch',
                'port_name_format': 'Ethernet{0}', 'port_segment_size': 0, 'symbol': ':/symbols/ethernet_switch.svg',
                'template_id': '1966b864-93e7-32d5-965f-001384eec461', 'x': x, 'y': y, 'z': z,
                'properties': {'ports_mapping': []}}

        portnum = port_number

        for i in range(portnum):
            data["properties"]["ports_mapping"].append(
                {'name': 'Ethernet' + str(i), 'port_number': i, 'type': 'access', 'vlan': 1})

        response = requests.post(url, json=data, auth=(username, password))

        if response.status_code == 201:
            json_data = response.json()
            return json_data["node_id"]
        else:
            print(f"Error create_switch: {response.status_code}")

    def create_ubuntu_base(self, x, y, z):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/nodes"

        data = {'name': 'UbuntuDesktopGuest20.04.4-{0}', 'node_type': 'qemu', "console_type": "vnc", "console": 5901, "height": 59,
                "port_name_format": "eth{0}",
                'properties': {"adapters": 11, "adapter_type": "virtio-net-pci", "ram": 2048,
                               "hda_disk_image": "Ubuntu 20.04.4 (64bit).vmdk",
                               'qemu_path': '/usr/bin/qemu-system-x86_64'},
                'compute_id': 'local', 'x': x, 'y': y, 'z': z,
                "symbol": ":/symbols/classic/qemu_guest.svg"}

        response = requests.post(url, json=data, auth=(username, password))

        if response.status_code == 201:
            json_data = response.json()
            return json_data["node_id"]
        else:
            print(f"Error create_ubuntu_base: {response.status_code}")
            print("Error message:", response.text)

    def duplicate_node(self, node_id, x, y, z):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/nodes/{node_id}/duplicate"
        data = {"x": x, "y": y, "z": z}
        response = requests.post(url, json=data, auth=(username, password))
        if response.status_code == 201:
            json_data = response.json()
            node_id = json_data["node_id"]
        else:
            print(f"Error duplicate_node: {response.status_code}")
        return node_id

    def get_links(self):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/links"

        response = requests.get(url, auth=(username, password))

        if response.status_code == 200:
            json_data = response.json()

            array = []
            for i in json_data:
                array.append([i["link_id"], i["nodes"][0]["node_id"], i["nodes"][1]["node_id"]])
            return array
        else:
            print(f"Error get_links: {response.status_code}")

    def start_capture_link(self, link_id):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/links/" + link_id + "/start_capture"

        response = requests.post(url, auth=(username, password))
        if response.status_code == 201:
            print("capture link " + link_id)
        else:
            print(f"Error start_capture_link: {response.status_code}")

    def stop_capture_link(self, link_id):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/links/" + link_id + "/stop_capture"

        response = requests.post(url, auth=(username, password))
        if response.status_code == 201:
            print("Stop capture link " + link_id)
        else:
            print(f"Error stop_capture_link: {response.status_code}")

    def nocapture(self):
        global PROJECT_ID
        array_links = self.get_links(PROJECT_ID)
        for link in array_links:
            self.stop_capture_link(link[0])

    def create_unikernel(self, x, y, z, type):
        global PROJECT_ID, username, password
        url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/nodes"

        unikernel_image = ""
        if type == HELLO_UNIKERNEL_TYPE:
            unikernel_image = "hello"

        data = {
            "command_line": "", "compute_id": "local", "console": 5005, "console_auto_start": False,
            "console_host": "localhost", "console_type": "telnet", "custom_adapters": [], "first_port_name": "",
            "height": 59, "label": {"rotation": 0,
                                    "style": "font-family: TypeWriter;font-size: 10.0;font-weight: bold;fill: #000000;fill-opacity: 1.0;",
                                    "text": unikernel_image + "-unikernel-mirageOS", "x": -150,
                                    "y": -25},
            "properties": {"adapter_type": "virtio-net-pci", "adapters": 2,
                           "hda_disk_image": unikernel_image + ".qcow2",
                           "hda_disk_interface": "ide", "process_priority": "normal",
                           "qemu_path": "/bin/qemu-system-x86_64", "ram": 32},
            "locked": False, "symbol": ":/symbols/qemu_guest.svg", "name": unikernel_image + "-unikernel-mirageOS",
            "node_type": "qemu", "width": 65, "x": x, "y": y, "z": z
        }

        response = requests.post(url, json=data, auth=(username, password))

        if response.status_code == 201:
            json_data = response.json()
            return json_data["node_id"]
        else:
            print(f"Error create" + unikernel_image + "_unikernel: {response.status_code}")
            print("Error message:", response.text)


class Node:
    port_free = 0
    adapter_free = 0
    port_size = 1
    adapter_size = 1
    type = 0

    def __init__(self, port_size, adapter_size, id, type):
        self.port_size = port_size
        self.adapter_size = adapter_size
        self.id = id
        self.type = type

    def update_adapter_free(self):
        self.adapter_free += 1

    def update_port_free(self):
        self.port_free += 1

    def check_port_free(self):
        if self.port_free >= self.port_size:
            print("Error: exceed maximum node port")
            sys.exit()

    def check_adapter_free(self):
        if self.adapter_free >= self.adapter_size:
            print("Error: exceed maximum node adapter")
            sys.exit()

    def connect(self, node):
        if not node:
            return

        adapter_number_1 = 0
        port_number_1 = 0
        adapter_number_2 = 0
        port_number_2 = 0

        if self.type == SWITCH_TYPE or self.type == WINDOW_TYPE or self.type == ROUTER_TYPE:
            self.check_port_free()
            port_number_1 = self.port_free
            self.update_port_free()
        elif self.type == UBUNTU_TYPE or self.type == HELLO_UNIKERNEL_TYPE or self.type == UBUNTU_BASE_TYPE:
            self.check_adapter_free()
            adapter_number_1 = self.adapter_free
            self.update_adapter_free()

        if node.type == SWITCH_TYPE or node.type == WINDOW_TYPE or node.type == ROUTER_TYPE:
            node.check_port_free()
            port_number_2 = node.port_free
            node.update_port_free()
        elif node.type == UBUNTU_TYPE or node.type == HELLO_UNIKERNEL_TYPE or node.type == UBUNTU_BASE_TYPE:
            node.check_adapter_free()
            adapter_number_2 = node.adapter_free
            node.update_adapter_free()

        gnsmanager.link_host_to_host(self.id, adapter_number_1, port_number_1, node.id, adapter_number_2, port_number_2)


class Ubuntu:
    adapter_free = 0
    adapter_size = MAX_SIZE_OF_UBUNTU_ADAPTER
    type = UBUNTU_TYPE

    def __init__(self, adapter_size, id):
        self.adapter_size = adapter_size
        self.id = id

    def update_adapter_free(self):
        self.adapter_free += 1

    def check_adapter_free(self):
        if self.adapter_free >= self.adapter_size:
            print("Error: exceed maximum ubuntu adapter")
            sys.exit()


class Switch:
    port_free = 0
    port_size = MAX_SIZE_OF_SWITCH_PORT
    type = SWITCH_TYPE

    def __init__(self, port_size, id):
        self.port_size = port_size
        self.id = id

    def update_port_free(self):
        self.port_free += 1

    def check_port_free(self):
        if self.port_free >= self.port_size:
            print("Error: exceed maximum switch port")
            sys.exit()


def duplicate_project(project_name):
    global PROJECT_ID, username, password
    url = "http://localhost:3080/v2/projects/" + PROJECT_ID + "/duplicate"
    data = {"name": project_name.replace(".gns3", "") + "-2"}

    response = requests.post(url, json=data, auth=(username, password))
    if response.status_code == 201:
        json_data = response.json()
        for project in json_data:
            if project["status"] == "opened":
                print(project["filename"])
                return project["project_id"], project["filename"]
    else:
        print(f"Error duplicate_project: {response.status_code}")


# def link_host(host_1, host_2):
#     if not host_1 or not host_2:
#         return
#     adapter_number_1 = 0
#     port_number_1 = 0
#     adapter_number_2 = 0
#     port_number_2 = 0
#
#     if host_1.type == SWITCH_TYPE or host_1.type == 0:
#         host_1.check_port_free()
#         port_number_1 = host_1.port_free
#         host_1.update_port_free()
#     elif host_1.type == UBUNTU_TYPE:
#         host_1.check_adapter_free()
#         adapter_number_1 = host_1.adapter_free
#         host_1.update_adapter_free()
#
#     if host_2.type == SWITCH_TYPE or host_2.type == 0:
#         host_2.check_port_free()
#         port_number_2 = host_2.port_free
#         host_2.update_port_free()
#     elif host_2.type == UBUNTU_TYPE:
#         host_2.check_adapter_free()
#         adapter_number_2 = host_2.adapter_free
#         host_2.update_adapter_free()
#
#     link_host_to_host(host_1.id, adapter_number_1, port_number_1, host_2.id, adapter_number_2, port_number_2)


def get_node(node_id):
    global PROJECT_ID, username, password

    url = f"http://localhost:3080/v2/projects/{PROJECT_ID}/nodes/{node_id}"
    response = requests.get(url, auth=(username, password))

    if response.status_code == 200:
        json_data = response.json()
        return json_data
    else:
        print(f"Error get_node: {response.status_code}")


def process_packet(pkt):
    global array_point, continue_sniffing, malware_instance, is_infected, map_infecting, map_infector
    if SMB_Header in pkt:
        if not is_infected and pkt[SMB_Header].Command == 0xa0 and pkt[SMB_Header].Flags == 0x18:
            victim_ip = pkt[IP].dst
            infecting_ip = pkt[IP].src
            if victim_ip not in map_infecting.keys():
                map_infecting[victim_ip] = [infecting_ip]
            elif infecting_ip not in map_infecting[victim_ip]:
                map_infecting[victim_ip].append(infecting_ip)
            OUTPUT_FILE = "infecting.txt"
            with open(OUTPUT_FILE, "w") as file:
                json.dump(map_infecting, file)

        if (malware_instance == "wannacry" and pkt[SMB_Header].MID == 81) or (
                malware_instance == "notpetya" and pkt[SMB_Header].Reserved == 0x0011):
            array_point.append(pkt[IP].src + "," + pkt[IP].dst)
            if pkt[IP].src not in map_infector:
                map_infector[pkt[IP].src] = 1
            else:
                input("Error" + pkt[IP].src)
            print("saved", pkt[IP].src, " - ", pkt[IP].dst, "- size", len(array_point))

            OUTPUT_FILE = "infector.txt"
            with open(OUTPUT_FILE, "w") as file:
                file.write(str(array_point))
            continue_sniffing = False
            is_infected = True


def extract_component_id(component):
    str = component.split("_")
    type = str[0]
    index_str = str[1]
    if "-" in index_str:
        start_host = int(index_str.split("-")[0])
        end_host = int(index_str.split("-")[1]) + 1
        return list(range(start_host, end_host)), type
    else:
        return [int(index_str)], type


if __name__ == "__main__":
    gnsmanager = GNS3Manager()

    PROJECT_ID, project_name = gnsmanager.get_projectId()
    user_input = input("Press enter if Correct project to rennder?")
    if user_input != '':
        sys.exit()

    if len(sys.argv) > 1:
        command = sys.argv[1]

        if command == "1":
            with open('network_config.txt', 'r') as file:
                lines = file.readlines()
            NUM_SWITCH = int(lines[0].strip())  # Number of switches
            NUM_HOST = int(lines[1].strip())  # Number of PCs
            NUM_HELLO_UNIKERNEL = int(lines[2].strip())  # Number of hello-unikernel
            NUM_UBUNTU_BASE = int(lines[3].strip())
            is_default = int(lines[4].strip())
            configuration_string = lines[5].strip()  # Configuration string
            print("Number of switches:", NUM_SWITCH)
            print("Number of PCs:", NUM_HOST)
            print("Number of Hello unikernel:", NUM_HELLO_UNIKERNEL)
            print("Number of ubuntu base:", NUM_UBUNTU_BASE)
            print("Is default configuration:", is_default)
            print("Configuration string:", configuration_string)
            configuration_string = configuration_string.replace(r'\n', '\n')

            NUM_DUPLICATE = NUM_HOST - 2

            array_nodes = gnsmanager.get_nodes()
            array_hosts = []
            array_switchs = []
            array_hello_unikernel = []
            array_ubuntu_base = []
            router_0 = Node(1, 1, "", ROUTER_TYPE)
            ubuntu_pc = Node(1, MAX_SIZE_OF_UBUNTU_ADAPTER, "", UBUNTU_TYPE)
            win_1_pc = Node(1, 1, "", WINDOW_TYPE)
            win_2_pc = Node(1, 1, "", WINDOW_TYPE)

            for node in array_nodes:
                if node[1] == "Windows7w/IE9-1" or node[1] == "Windows7w/IE10-1":
                    win_1_pc.id = node[0]
                    x = node[2]
                    y = node[3]
                    z = node[4]
                    array_hosts.append(win_1_pc)
                elif node[1] == "Windows7w/IE9-2" or node[1] == "Windows7w/IE10-2":
                    win_2_pc.id = node[0]
                    x = node[2]
                    y = node[3]
                    z = node[4]
                    array_hosts.append(win_2_pc)
                elif node[1] == "R1":
                    router_0.id = node[0]
                    x = node[2]
                    y = node[3]
                    z = node[4]
                    gnsmanager.start_node(router_0.id)
                elif node[1] == "UbuntuDesktopGuest18.04.6-1":
                    ubuntu_pc.id = node[0]
                    x = node[2]
                    y = node[3]
                    z = node[4]
                    gnsmanager.start_node(ubuntu_pc.id)
                elif node[1] == "UbuntuDesktopGuest20.04.4-1":
                    x = node[2]
                    y = node[3]
                    z = node[4]
                    array_ubuntu_base.append(Node(1, 11, node[0], UBUNTU_BASE_TYPE))
            # Create switches
            x_local = -500
            y_local = -200
            step = 0
            for i in range(NUM_SWITCH):
                x_temp = x_local + step
                step += 150
                switch_id = gnsmanager.create_switch(MAX_SIZE_OF_SWITCH_PORT, x_temp, y_local, 1)
                array_switchs.append(Node(MAX_SIZE_OF_SWITCH_PORT, 1, switch_id, SWITCH_TYPE))

            # Create Windows hosts
            x_local = -1500
            y_local = 0
            step = 100
            for i in range(2, NUM_HOST):
                j = i // 14
                y_temp = y_local + step * j
                x_temp = x_local + step * 2 * (i % 14)
                array_hosts.append(Node(1, 1, gnsmanager.duplicate_node(win_2_pc.id, x_temp, y_temp, 1), WINDOW_TYPE))

            # create hello unikernel
            x_local = 1000
            y_local = -200
            step = 100
            for i in range(NUM_HELLO_UNIKERNEL):
                y_temp = y_local + step * i
                x_temp = x_local
                array_hello_unikernel.append(
                    Node(1, 2, gnsmanager.create_unikernel(x_temp, y_temp, 1, HELLO_UNIKERNEL_TYPE),
                         HELLO_UNIKERNEL_TYPE))

            # create ubuntu base
            x_local = -300
            y_local = 0
            step = 100
            for i in range(1, NUM_UBUNTU_BASE):
                y_temp = y_local + step * i
                x_temp = x_local
                array_ubuntu_base.append(
                    Node(1, 11, gnsmanager.create_ubuntu_base(x_temp, y_temp, 1), UBUNTU_BASE_TYPE))

            if is_default:
                configuration_string = "sw_0,pc_0-49\nsw_1,pc_50-99\nsw,sw\nsw_0,r_0\nsw_0,u_0\nub_0,ub_1"
            else:
                if configuration_string == "":
                    sys.exit()

            connection_cmd_split = configuration_string.split("\n")
            for connection_cmd in connection_cmd_split:
                components = connection_cmd.split(",")
                if components[0] == "sw" and components[1] == "sw":
                    for i in range(NUM_SWITCH - 1):
                        switch_1 = array_switchs[i]
                        switch_2 = array_switchs[i + 1]
                        switch_1.connect(switch_2)
                else:
                    host1_index, host1_type = extract_component_id(components[0])
                    host2_index, host2_type = extract_component_id(components[1])
                    for index1 in host1_index:
                        host1 = ""
                        if index1 == 0 and host1_type == "u":
                            host1 = ubuntu_pc
                        elif index1 == 0 and host1_type == "r":
                            host1 = router_0
                        elif host1_type == "pc":
                            host1 = array_hosts[index1]
                        elif host1_type == "sw":
                            host1 = array_switchs[index1]
                        elif host1_type == "uhe":
                            host1 = array_hello_unikernel[index1]
                        elif host1_type == "ub":
                            host1 = array_ubuntu_base[index1]

                        for index2 in host2_index:
                            host2 = ""
                            if index2 == 0 and host2_type == "u":
                                host2 = ubuntu_pc
                            elif index2 == 0 and host2_type == "r":
                                host2 = router_0
                            elif host2_type == "pc":
                                host2 = array_hosts[index2]
                            elif host2_type == "sw":
                                host2 = array_switchs[index2]
                            elif host2_type == "uhe":
                                host2 = array_hello_unikernel[index2]
                            elif host2_type == "ub":
                                host2 = array_ubuntu_base[index2]

                            print(host1_type, index1, "connect", host2_type, index2)
                            host1.connect(host2)

            # Capture packets from communications/links
            if input("Capture link (y/N)?") == "y":
                array_links = gnsmanager.get_links()
                for link in array_links:
                    if ubuntu_pc.id not in link and router_0.id not in link and win_1_pc.id not in link:
                        gnsmanager.start_capture_link(link[0])

            # Press any key to start Windows PCs
            if input("Start Windows PCs (y/N)?") != "y":
                sys.exit()
            for host in array_hosts:
                time.sleep(1)
                gnsmanager.start_node(host.id)

            # TCP listen on port 12345 to collect propagation time data
            command = "nc -l -p 12345 > measurement.txt"
            try:
                subprocess.run(command, shell=True, check=True)
                print("Command executed successfully.")
                gnsmanager.nocapture()  # Stop capturing packets after receiving data
                # Store data in a .zip
                subprocess.run("zip a.zip ~/GNS3/projects/" + project_name.replace(".gns3", "") + "/project-files/captures/*", shell=True, check=True)
            except subprocess.CalledProcessError as e:
                print(f"Error executing command: {e}")
        elif command == "node":
            node_id = sys.argv[2]
            nodes = gnsmanager.get_nodes()
            for n in nodes:
                if node_id in n[0]:
                    print(n[1])
            sys.exit()
        elif command == "duplicate":
            PROJECT_ID = sys.argv[2]
            duplicate_project(project_name)
            sys.exit()
        elif command == "capture":
            array_links = gnsmanager.get_links()
            for link in array_links:
                gnsmanager.start_capture_link(link[0])
            sys.exit()
        elif command == "nocapture":
            gnsmanager.nocapture()
        elif command == "read":
            malware_instance = sys.argv[2]
            # folder_path_capture = "/home/anh/GNS3/projects/" + project_name.replace(".gns3","") + "/project-files/captures/"
            folder_path_capture = "/mnt/raid5/projects/" + project_name.replace(".gns3",
                                                                                "") + "/project-files/captures/"
            files = os.listdir(folder_path_capture)
            pcap_files = [(folder_path_capture + file) for file in files if file.endswith('.pcap')]
            for pcap_file in pcap_files:
                continue_sniffing = True
                is_infected = False
                sniff(offline=pcap_file, filter="port 445", prn=process_packet,
                      stop_filter=lambda _: not continue_sniffing, verbose=False)
            sys.exit()
    else:
        # gnsmanager.create_hello_unikernel(0, 0, 1)
        gnsmanager.create_ubuntu_base(0, 0, 1)
