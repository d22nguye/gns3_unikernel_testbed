let log = Logs.Src.create "rules" ~doc:"IDS rules management"

module Log = (val Logs.src_log log : Logs.LOG)

type decision = NORMAL | ATTACKER
(* TODO:
   | ESTABLISHED
   | RELATED will need more work... *)

type rule = {
  src : Ipaddr.V4.Prefix.t;
  psrc : int;
  dst : Ipaddr.V4.Prefix.t;
  pdst : int;
  proto : Ipv4_packet.protocol option;
  action : decision option;
}

(* A match is either the target is joker or the ip is in the target network *)
let match_ip ip target =
  target = Ipaddr.V4.Prefix.global || Ipaddr.V4.Prefix.mem ip target

type t = { mutable l : rule list; default : bool }

let init default =
  {
    l =
      [
        {
          src = Ipaddr.V4.Prefix.global;
          psrc = 0;
          dst = Ipaddr.V4.Prefix.global;
          pdst = 12345;
          proto = Some `TCP;
          action = Some ATTACKER;
        };
        {
          src = Ipaddr.V4.Prefix.global;
          psrc = 0;
          dst = Ipaddr.V4.Prefix.global;
          pdst = 0;
          proto = None;
          action = Some NORMAL;
        };
      ];
    default;
  }

let is_matching_port packet proto r_psrc r_pdst =
  match (r_psrc, r_pdst, proto) with
  | 0, 0, _ -> true
  | _, _, 6 -> (
      (* TCP *)
      match Tcp.Tcp_packet.Unmarshal.of_cstruct packet with
      | Result.Error s ->
          Logs.err (fun m -> m "Can't parse TCP packet: %s" s);
          false
      | Result.Ok (tcp_hdr, _payload) ->
          (r_psrc = 0 || tcp_hdr.src_port = r_psrc)
          && (r_pdst = 0 || tcp_hdr.dst_port = r_pdst))
  | _, _, 17 -> (
      (* UDP *)
      match Udp_packet.Unmarshal.of_cstruct packet with
      | Result.Error s ->
          Logs.err (fun m -> m "Can't parse UDP packet: %s" s);
          false
      | Result.Ok (udp_hdr, _payload) ->
          (r_psrc = 0 || udp_hdr.src_port = r_psrc)
          && (r_pdst = 0 || udp_hdr.dst_port = r_pdst))
  | _, _, 1 -> true (* ICMP *)
  | _ -> true

(* Takes an ipv4 header [ipv4_hdr] and the whole IPv4 [packet] (containing the header).
   We want to filter out any packet matching the [filters] list, and if not filtered,
   transfer it (unchanged) to [forward_to].
   NOTE: We know the packet is not for us. *)
let is_attacker t (ipv4_hdr, packet) =
  let rec apply_rules : bool -> Ipv4_packet.t * Cstruct.t -> rule list -> bool =
   fun default (ipv4_hdr, packet) ids_rules ->
    match ids_rules with
    (* If the list is empty -> apply default (true or false) action *)
    | [] -> default
    (* If the packet matches the condition and has an accept action *)
    | { src; psrc; dst; pdst; proto; action = Some ATTACKER } :: _
      when match_ip ipv4_hdr.src src && match_ip ipv4_hdr.dst dst
           && (proto = None || Ipv4_packet.Unmarshal.int_to_protocol ipv4_hdr.Ipv4_packet.proto = proto) 
           && is_matching_port packet ipv4_hdr.Ipv4_packet.proto psrc pdst ->
        Log.debug (fun f -> f "Accept a packet from %a to %a..." Ipaddr.V4.pp ipv4_hdr.src Ipaddr.V4.pp ipv4_hdr.dst);
        true
    (* Otherwise the packet matches and the action is drop *)
    | { src; psrc; dst; pdst; proto; action = Some NORMAL } :: _
      when match_ip ipv4_hdr.src src && match_ip ipv4_hdr.dst dst
           && (proto = None || Ipv4_packet.Unmarshal.int_to_protocol ipv4_hdr.Ipv4_packet.proto = proto)
           && is_matching_port packet ipv4_hdr.Ipv4_packet.proto psrc pdst ->
        Log.debug (fun f -> f "Filter out a packet from %a to %a..." Ipaddr.V4.pp ipv4_hdr.src Ipaddr.V4.pp ipv4_hdr.dst);
        false
    (* Or finally the packet does not match the condition *)
    | _ :: tail -> apply_rules default (ipv4_hdr, packet) tail
  in

  apply_rules t.default (ipv4_hdr, packet) t.l
  