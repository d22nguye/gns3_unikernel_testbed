(* mirage >= 4.7.0 & < 4.8.0 *)
open Mirage

(* we need two network interfaces: a public side and a private side *)
(* a bit of magic: currently, multiple networks only work on Unix and Xen
   backends, so we can get away with this indexes-as-numbers-as-strings
   silliness.
   See https://github.com/mirage/mirage/issues/645 *)
let public_netif4 =
  Key.(if_impl is_solo5
         (netif ~group:"public4" "public4")
         (netif ~group:"public4" "0"))
let private_netif4 =
  Key.(if_impl is_solo5
         (netif ~group:"private4" "private4")
         (netif ~group:"private4" "1"))

let public_netif5 =
   Key.(if_impl is_solo5
            (netif ~group:"public5" "public5")
            (netif ~group:"public5" "0"))
   
   let private_netif5 =
   Key.(if_impl is_solo5
            (netif ~group:"private5" "private5")
            (netif ~group:"private5" "1"))
   
   let public_netif6 =
   Key.(if_impl is_solo5
            (netif ~group:"public6" "public6")
            (netif ~group:"public6" "0"))
   
   let private_netif6 =
   Key.(if_impl is_solo5
            (netif ~group:"private6" "private6")
            (netif ~group:"private6" "1"))
   
   let public_netif7 =
   Key.(if_impl is_solo5
            (netif ~group:"public7" "public7")
            (netif ~group:"public7" "0"))
   
   let private_netif7 =
   Key.(if_impl is_solo5
            (netif ~group:"private7" "private7")
            (netif ~group:"private7" "1"))
   
   let public_netif8 =
   Key.(if_impl is_solo5
            (netif ~group:"public8" "public8")
            (netif ~group:"public8" "0"))
   
   let private_netif8 =
   Key.(if_impl is_solo5
            (netif ~group:"private8" "private8")
            (netif ~group:"private8" "1"))
   
   let public_netif9 =
   Key.(if_impl is_solo5
            (netif ~group:"public9" "public9")
            (netif ~group:"public9" "0"))
   
   let private_netif9 =
   Key.(if_impl is_solo5
            (netif ~group:"private9" "private9")
            (netif ~group:"private9" "1"))
   
   let public_netif10 =
   Key.(if_impl is_solo5
            (netif ~group:"public10" "public10")
            (netif ~group:"public10" "0"))
   
   let private_netif10 =
   Key.(if_impl is_solo5
            (netif ~group:"private10" "private10")
            (netif ~group:"private10" "1"))
   
   let public_netif11 =
   Key.(if_impl is_solo5
            (netif ~group:"public11" "public11")
            (netif ~group:"public11" "0"))
   
   let private_netif11 =
   Key.(if_impl is_solo5
            (netif ~group:"private11" "private11")
            (netif ~group:"private11" "1"))
   
   let public_netif12 =
   Key.(if_impl is_solo5
            (netif ~group:"public12" "public12")
            (netif ~group:"public12" "0"))
   
   let private_netif12 =
   Key.(if_impl is_solo5
            (netif ~group:"private12" "private12")
            (netif ~group:"private12" "1"))
   
   let public_netif13 =
   Key.(if_impl is_solo5
            (netif ~group:"public13" "public13")
            (netif ~group:"public13" "0"))
   
   let private_netif13 =
   Key.(if_impl is_solo5
            (netif ~group:"private13" "private13")
            (netif ~group:"private13" "1"))
          

(* build ethernet interfaces on top of those network interfaces *)
let public_ethernet4 = ethif public_netif4
let private_ethernet4 = ethif private_netif4

let public_ethernet5 = ethif public_netif5
let private_ethernet5 = ethif private_netif5

let public_ethernet6 = ethif public_netif6
let private_ethernet6 = ethif private_netif6

let public_ethernet7 = ethif public_netif7
let private_ethernet7 = ethif private_netif7

let public_ethernet8 = ethif public_netif8
let private_ethernet8 = ethif private_netif8

let public_ethernet9 = ethif public_netif9
let private_ethernet9 = ethif private_netif9

let public_ethernet10 = ethif public_netif10
let private_ethernet10 = ethif private_netif10

let public_ethernet11 = ethif public_netif11
let private_ethernet11 = ethif private_netif11

let public_ethernet12 = ethif public_netif12
let private_ethernet12 = ethif private_netif12

let public_ethernet13 = ethif public_netif13
let private_ethernet13 = ethif private_netif13


let packages = [
  package "ethernet";
  package "ipaddr";
  package ~min:"8.2.0" ~sublibs:["ipv4";"tcp"] "tcpip"; (* There was a breaking API changes in the 8.2 series *)
  package "logs";
  (* package ~min:"4.0.0" "mirage-runtime"; *) (* We want to avoid runtime argument at any cost *)
]

(* our unikernel needs to know about physical network, and ethernet modules
   for each interface. Even though these modules will be the same for both
   interfaces in our case, we have to pass them separately. *)
let main = main "Unikernel.Main" ~packages
           (network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            network  @-> network  @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            ethernet @-> ethernet @->
            random   @-> mclock   @-> job)

(* we need to pass each of the network-related impls we've made to the
   unikernel, so that it can start the appropriate listeners. *)
let () = register "simple-dns" [ main
                              $ public_netif4    $ private_netif4
                              $ public_netif5    $ private_netif5
                              $ public_netif6    $ private_netif6
                              $ public_netif7    $ private_netif7
                              $ public_netif8    $ private_netif8
                              $ public_netif9    $ private_netif9
                              $ public_netif10   $ private_netif10
                              $ public_netif11   $ private_netif11
                              $ public_netif12   $ private_netif12
                              $ public_netif13   $ private_netif13
                              $ public_ethernet4 $ private_ethernet4
                              $ public_ethernet5 $ private_ethernet5
                              $ public_ethernet6 $ private_ethernet6
                              $ public_ethernet7 $ private_ethernet7
                              $ public_ethernet8 $ private_ethernet8
                              $ public_ethernet9 $ private_ethernet9
                              $ public_ethernet10 $ private_ethernet10
                              $ public_ethernet11 $ private_ethernet11
                              $ public_ethernet12 $ private_ethernet12
                              $ public_ethernet13 $ private_ethernet13
                              $ default_random  $ default_monotonic_clock
                              ]