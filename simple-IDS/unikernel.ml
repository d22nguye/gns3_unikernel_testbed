(* This unikernel is largely inspired by the example at https://github.com/mirage/mirage-nat/ *)

open Lwt.Infix

module Main
    (* our unikernel is functorized over the physical, and ethernet modules
       for the public and private interfaces, so each one shows up as a module
       argument. *)
      (Public_net4: Mirage_net.S) (Private_net4: Mirage_net.S)
      (Public_net5: Mirage_net.S) (Private_net5: Mirage_net.S)
      (Public_net6: Mirage_net.S) (Private_net6: Mirage_net.S)
      (Public_net7: Mirage_net.S) (Private_net7: Mirage_net.S)
      (Public_net8: Mirage_net.S) (Private_net8: Mirage_net.S)
      (Public_net9: Mirage_net.S) (Private_net9: Mirage_net.S)
      (Public_net10: Mirage_net.S) (Private_net10: Mirage_net.S)
      (Public_net11: Mirage_net.S) (Private_net11: Mirage_net.S)
      (Public_net12: Mirage_net.S) (Private_net12: Mirage_net.S)
      (Public_net13: Mirage_net.S) (Private_net13: Mirage_net.S)
      (Public_ethernet4 : Ethernet.S) (Private_ethernet4 : Ethernet.S)
      (Public_ethernet5 : Ethernet.S) (Private_ethernet5 : Ethernet.S)
      (Public_ethernet6 : Ethernet.S) (Private_ethernet6 : Ethernet.S)
      (Public_ethernet7 : Ethernet.S) (Private_ethernet7 : Ethernet.S)
      (Public_ethernet8 : Ethernet.S) (Private_ethernet8 : Ethernet.S)
      (Public_ethernet9 : Ethernet.S) (Private_ethernet9 : Ethernet.S)
      (Public_ethernet10 : Ethernet.S) (Private_ethernet10 : Ethernet.S)
      (Public_ethernet11 : Ethernet.S) (Private_ethernet11 : Ethernet.S)
      (Public_ethernet12 : Ethernet.S) (Private_ethernet12 : Ethernet.S)
      (Public_ethernet13 : Ethernet.S) (Private_ethernet13 : Ethernet.S)
      (Random : Mirage_crypto_rng_mirage.S) (Clock : Mirage_clock.MCLOCK)
  = struct

  (* configure logs, so we can use them later *)
  let log = Logs.Src.create "ids" ~doc:"IDS device"
  module Log = (val Logs.src_log log : Logs.LOG)

  (* the specific impls we're using show up as arguments to start. *)
  let start public_netif4 private_netif4
            public_netif5 private_netif5
            public_netif6 private_netif6
            public_netif7 private_netif7
            public_netif8 private_netif8
            public_netif9 private_netif9
            public_netif10 private_netif10
            public_netif11 private_netif11
            public_netif12 private_netif12
            public_netif13 private_netif13
            _public_ethernet4 private_ethernet4
            _public_ethernet5 _private_ethernet5
            _public_ethernet6 _private_ethernet6
            _public_ethernet7 _private_ethernet7
            _public_ethernet8 _private_ethernet8
            _public_ethernet9 _private_ethernet9
            _public_ethernet10 _private_ethernet10
            _public_ethernet11 _private_ethernet11
            _public_ethernet12 _private_ethernet12
            _public_ethernet13 _private_ethernet13
            _rng () =

    (* Creates a set of rules (empty) and a default action (claim a packet is not an attack) *)
    let ids_rules =
      Rules.init false
    in

    let output_public4 packet =
      let len = Cstruct.length packet in
      Public_net4.write public_netif4 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net4.pp_error e) ;
        ()
    in

    let output_public5 packet =
      let len = Cstruct.length packet in
      Public_net5.write public_netif5 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net5.pp_error e) ;
        ()      
    in

    let output_public6 packet =
      let len = Cstruct.length packet in
      Public_net6.write public_netif6 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net6.pp_error e) ;
        ()
    in

    let output_public7 packet =
      let len = Cstruct.length packet in
      Public_net7.write public_netif7 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net7.pp_error e) ;
        ()
    in
    
    let output_public8 packet =
      let len = Cstruct.length packet in
      Public_net8.write public_netif8 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net8.pp_error e) ;
        ()
    in
    
    let output_public9 packet =
      let len = Cstruct.length packet in
      Public_net9.write public_netif9 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net9.pp_error e) ;
        ()
    in
    
    let output_public10 packet =
      let len = Cstruct.length packet in
      Public_net10.write public_netif10 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net10.pp_error e) ;
        ()
    in
    
    let output_public11 packet =
      let len = Cstruct.length packet in
      Public_net11.write public_netif11 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net11.pp_error e) ;
        ()
    in
    
    let output_public12 packet =
      let len = Cstruct.length packet in
      Public_net12.write public_netif12 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net12.pp_error e) ;
        ()
    in
    
    let output_public13 packet =
      let len = Cstruct.length packet in
      Public_net13.write public_netif13 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Public_net13.pp_error e) ;
        ()
    in

    (* Forward the (dest, packet) [packet] to the private interface, using [dest] to understand how to route *)
    let output_private4 packet =
      (* For IPv4 only one prefix can be configured so the list is always of length 1 *)
        let len = Cstruct.length packet in
        Private_net4.write private_netif4 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
        | Ok () -> ()
        | Error e ->
          Log.warn (fun f -> f "netif write errored %a" Private_net4.pp_error e) ;
          ()
    in
    
    let output_private5 packet =
      let len = Cstruct.length packet in
      Private_net5.write private_netif5 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net5.pp_error e) ;
        ()
    in
    
    let output_private6 packet =
      let len = Cstruct.length packet in
      Private_net6.write private_netif6 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net6.pp_error e) ;
        ()
    in
    
    let output_private7 packet =
      let len = Cstruct.length packet in
      Private_net7.write private_netif7 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net7.pp_error e) ;
        ()
    in
    
    let output_private8 packet =
      let len = Cstruct.length packet in
      Private_net8.write private_netif8 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net8.pp_error e) ;
        ()
    in
    
    let output_private9 packet =
      let len = Cstruct.length packet in
      Private_net9.write private_netif9 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net9.pp_error e) ;
        ()
    in
    
    let output_private10 packet =
      let len = Cstruct.length packet in
      Private_net10.write private_netif10 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net10.pp_error e) ;
        ()
    in
    
    let output_private11 packet =
      let len = Cstruct.length packet in
      Private_net11.write private_netif11 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net11.pp_error e) ;
        ()
    in
    
    let output_private12 packet =
      let len = Cstruct.length packet in
      Private_net12.write private_netif12 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net12.pp_error e) ;
        ()
    in
    
    let output_private13 packet =
      let len = Cstruct.length packet in
      Private_net13.write private_netif13 ~size:len (fun b -> Cstruct.blit packet 0 b 0 len ; len) >|= function
      | Ok () -> ()
      | Error e ->
        Log.warn (fun f -> f "netif write errored %a" Private_net13.pp_error e) ;
        ()
    in

    let output_ether_private4 mac_src mac_dst packet =
      Private_ethernet4.write private_ethernet4 ~src:mac_src mac_dst `IPv4 (fun b ->
        let len = Cstruct.length packet in
        Cstruct.blit packet 0 b 0 len ;len) >>= function
        | Error e ->
          Log.err (fun f -> f "Failed to send packet from private interface: %a" Private_ethernet4.pp_error e);
          Lwt.return_unit
        | Ok () -> Lwt.return_unit

    in
    
    (* Takes an IPv4 [packet], unmarshal it, examine to detect attackers' IPs in TCP packets*)
    let detect_and_output_private4 packet header frame =
      (* Handle IPv4 only... *)
      match Ipv4_packet.Unmarshal.of_cstruct packet with
      | Result.Error s ->
        Logs.err (fun m -> m "Can't parse IPv4 packet: %s" s);
        Lwt.return_unit
      (* Otherwise unmarshal it, match rules with IPs, ports, proto *)
      | Result.Ok (ipv4_hdr, payload) ->
        match Rules.is_attacker ids_rules (ipv4_hdr, payload) with
        | false -> output_private4 frame
        | true ->  
          let options = ipv4_hdr.options in
          let len_payload = Cstruct.length payload in
          let new_options_cs = Custom_option.add_custom_option options in
          let new_ipv4_hdr = {ipv4_hdr with options= new_options_cs} in
          let new_ipv4_hdr_cs = Ipv4_packet.Marshal.make_cstruct ~payload_len:len_payload new_ipv4_hdr in
          let new_packet = Cstruct.append new_ipv4_hdr_cs payload in
          output_ether_private4 header.Ethernet.Packet.source header.Ethernet.Packet.destination new_packet
    in
    

    (* we need to establish listeners for the and public interfaces *)
    (* we're interested in all traffic to the physical interface; we'd like to
       send ARP traffic to the normal ARP listener and responder,
       handle ipv4 traffic with the functions we've defined above for detection,
       and ignore all ipv6 traffic. *)
    let listen_public4 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame = (* Takes an ethernet packet and send it to the relevant callback *)
          match Ethernet.Packet.of_cstruct frame with
          | Ok (header, payload) ->
            begin
              match header.Ethernet.Packet.ethertype with
              | `ARP -> output_private4 frame
              | `IPv4 -> detect_and_output_private4 payload header frame
              | _ -> Lwt.return_unit
            end
          | Error s ->
            Log.debug (fun f -> f "dropping Ethernet frame: %s" s);
            Lwt.return_unit
      in
      Public_net4.listen ~header_size public_netif4 input >>= function
      | Error e -> Log.debug (fun f -> f "public4 interface stopped: %a" Public_net4.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public4 interface terminated normally");
        Lwt.return_unit
    in
    
    let listen_public5 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private5 frame
      in
      Public_net5.listen ~header_size public_netif5 input >>= function
      | Error e -> Log.debug (fun f -> f "public5 interface stopped: %a" Public_net5.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public5 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public6 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private6 frame
      in
      Public_net6.listen ~header_size public_netif6 input >>= function
      | Error e -> Log.debug (fun f -> f "public6 interface stopped: %a" Public_net6.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public6 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public7 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private7 frame
      in
      Public_net7.listen ~header_size public_netif7 input >>= function
      | Error e -> Log.debug (fun f -> f "public7 interface stopped: %a" Public_net7.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public7 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public8 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private8 frame
      in
      Public_net8.listen ~header_size public_netif8 input >>= function
      | Error e -> Log.debug (fun f -> f "public8 interface stopped: %a" Public_net8.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public8 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public9 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private9 frame
      in
      Public_net9.listen ~header_size public_netif9 input >>= function
      | Error e -> Log.debug (fun f -> f "public9 interface stopped: %a" Public_net9.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public9 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public10 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private10 frame
      in
      Public_net10.listen ~header_size public_netif10 input >>= function
      | Error e -> Log.debug (fun f -> f "public10 interface stopped: %a" Public_net10.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public10 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public11 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private11 frame
      in
      Public_net11.listen ~header_size public_netif11 input >>= function
      | Error e -> Log.debug (fun f -> f "public11 interface stopped: %a" Public_net11.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public11 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public12 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private12 frame
      in
      Public_net12.listen ~header_size public_netif12 input >>= function
      | Error e -> Log.debug (fun f -> f "public12 interface stopped: %a" Public_net12.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public12 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_public13 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_private13 frame
      in
      Public_net13.listen ~header_size public_netif13 input >>= function
      | Error e -> Log.debug (fun f -> f "public13 interface stopped: %a" Public_net13.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "public13 interface terminated normally"); Lwt.return_unit
    in

    let listen_private4 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame = (* Takes an ethernet packet and send it to the relevant callback *)
          output_public4 frame
      in
      Private_net4.listen ~header_size private_netif4 input >>= function
      | Error e -> Log.debug (fun f -> f "private4 interface stopped: %a" Private_net4.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private4 interface terminated normally");
        Lwt.return_unit
    in

    let listen_private5 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public5 frame
      in
      Private_net5.listen ~header_size private_netif5 input >>= function
      | Error e -> Log.debug (fun f -> f "private5 interface stopped: %a" Private_net5.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private5 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private6 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public6 frame
      in
      Private_net6.listen ~header_size private_netif6 input >>= function
      | Error e -> Log.debug (fun f -> f "private6 interface stopped: %a" Private_net6.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private6 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private7 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public7 frame
      in
      Private_net7.listen ~header_size private_netif7 input >>= function
      | Error e -> Log.debug (fun f -> f "private7 interface stopped: %a" Private_net7.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private7 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private8 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public8 frame
      in
      Private_net8.listen ~header_size private_netif8 input >>= function
      | Error e -> Log.debug (fun f -> f "private8 interface stopped: %a" Private_net8.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private8 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private9 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public9 frame
      in
      Private_net9.listen ~header_size private_netif9 input >>= function
      | Error e -> Log.debug (fun f -> f "private9 interface stopped: %a" Private_net9.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private9 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private10 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public10 frame
      in
      Private_net10.listen ~header_size private_netif10 input >>= function
      | Error e -> Log.debug (fun f -> f "private10 interface stopped: %a" Private_net10.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private10 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private11 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public11 frame
      in
      Private_net11.listen ~header_size private_netif11 input >>= function
      | Error e -> Log.debug (fun f -> f "private11 interface stopped: %a" Private_net11.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private11 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private12 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public12 frame
      in
      Private_net12.listen ~header_size private_netif12 input >>= function
      | Error e -> Log.debug (fun f -> f "private12 interface stopped: %a" Private_net12.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private12 interface terminated normally"); Lwt.return_unit
    in
    
    let listen_private13 =
      let header_size = Ethernet.Packet.sizeof_ethernet
      and input frame =
        output_public13 frame
      in
      Private_net13.listen ~header_size private_netif13 input >>= function
      | Error e -> Log.debug (fun f -> f "private13 interface stopped: %a" Private_net13.pp_error e); Lwt.return_unit
      | Ok () -> Log.debug (fun f -> f "private13 interface terminated normally"); Lwt.return_unit
    in
    

    (* start both listeners, and continue as long as both are working. *)
    Lwt.pick [
      listen_public4;
      listen_public5;
      listen_public6;
      listen_public7;
      listen_public8;
      listen_public9;
      listen_public10;
      listen_public11;
      listen_public12;
      listen_public13;

      listen_private4;
      listen_private5;
      listen_private6;
      listen_private7;
      listen_private8;
      listen_private9;
      listen_private10;
      listen_private11;
      listen_private12;
      listen_private13;
    ]
end